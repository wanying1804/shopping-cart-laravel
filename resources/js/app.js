import Vue          from 'vue';
import ElementUI    from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import locale       from 'element-ui/lib/locale/lang/en';

Vue.use(ElementUI, { locale });

Vue.component('shopping-cart', require('./components/ShoppingCart.vue').default);
Vue.component('product-list', require('./components/ProductList.vue').default);

new Vue({
    el: '#app',
});
