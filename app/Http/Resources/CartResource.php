<?php
namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class CartResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id'           => $this->id,
            'total_price'  => $this->resource->getTotalPrice(),
            'products'     => CartProductResource::collection($this->resource->products),
        ];
    }
}
