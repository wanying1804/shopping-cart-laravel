<?php
namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class CartProductResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id'          => $this->id,
            'name'        => $this->name,
            'price'       => $this->price,
            'count'       => $this->resource->pivot->count,
            'total_price' => $this->resource->pivot->count * $this->price
        ];
    }
}
