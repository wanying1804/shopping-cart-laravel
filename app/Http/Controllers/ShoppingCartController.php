<?php

namespace App\Http\Controllers;

use App\Http\Resources\CartResource;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Class ShoppingCartController
 * @package App\Http\Controllers
 */
class ShoppingCartController extends Controller
{
    /**
     * @return CartResource
     */
    public function getProductsInUserCart()
    {
        return new CartResource(Auth::user()->shoppingCart);
    }

    /**
     * @param Product $product
     * @return \Illuminate\Http\JsonResponse
     */
    public function addProductToUserCart(Product $product)
    {
        $cart = Auth::user()->shoppingCart;

        $count = 1;
        if($existingProduct = $cart->products->find($product->id)) {
            $count = $existingProduct->pivot->count + 1;
        }

        $cart->products()->syncWithoutDetaching([$product->id => ['count' => $count]]);

        return response()->json([
            'message' => $product->name .' is added',
        ]);
    }

    /**
     * @param Request $request
     * @param Product $product
     * @return \Illuminate\Http\JsonResponse
     */
    public function updatedProductNumberInUserCart(Request $request, Product $product)
    {
        $cart = Auth::user()->shoppingCart;

        $cart->products()->findOrFail($product->id);

        $cart->products()->syncWithoutDetaching([$product->id => ['count' => $request->count]]);

        return response()->json([
            'message' => $product->name .' count is updated',
        ]);
    }

    /**
     * @param Product $product
     * @return \Illuminate\Http\JsonResponse
     */
    public function removeProductInUserCart(Product $product)
    {
        $cart = Auth::user()->shoppingCart;
        $cart->products()->detach($product->id);

        return response()->json([
            'message' => $product->name .' is removed',
        ]);
    }
}
