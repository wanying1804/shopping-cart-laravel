<?php

namespace App\Models;

use App\Observers\ShoppingCartObserver;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ShoppingCart
 * @package App\Models
 */
class ShoppingCart extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
    ];

    public static function boot()
    {
        parent::boot();
        static::observe(new ShoppingCartObserver());
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->hasOne(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function products()
    {
        return $this->belongsToMany(Product::class, 'x_shopping_cart_product')
            ->withPivot('count');
    }

    /**
     * @return string
     */
    public function getTotalPrice()
    {
        $prices =  $this->products->map(function ($product) {
            return number_format($product->price * $product->pivot->count, 2);
        });

        return number_format($prices->sum(), 2);
    }
}
