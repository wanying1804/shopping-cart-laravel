<?php
namespace App\Observers;

use App\Models\ShoppingCart;

/**
 * Class ShoppingCartObserver
 * @package App\Observers
 */
class ShoppingCartObserver
{
    /**
     * @param ShoppingCart $shoppingCart
     */
    public function deleted(ShoppingCart $shoppingCart)
    {
        $shoppingCart->products()->detach();
    }
}
