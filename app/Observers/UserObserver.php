<?php
namespace App\Observers;

use App\Models\User;

/**
 * Class UserObserver
 * @package App\Observers
 */
class UserObserver
{
    /**
     * @param User $user
     */
    public function created(User $user)
    {
        $user->shoppingCart()->firstOrCreate([]);
    }

    /**
     * @param User $user
     */
    public function deleted(User $user)
    {
        $user->shoppingCart()->delete();
    }
}
