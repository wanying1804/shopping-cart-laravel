VERSION ?= $(shell git describe --abbrev=0 --tags | sed -e 's/^v\(.*\)/\1/')

start:
	docker-compose up -d
	docker exec -t shopping-cart-web composer install
	docker exec -t shopping-cart-web php artisan migrate:fresh --seed

stop:
	docker-compose down

restart-js:
	docker restart shopping-cart-js

exec:
	docker exec -it shopping-cart-web bash

fresh:
	docker exec -t shopping-cart-web composer dump-autoload
	docker exec -t shopping-cart-web php artisan migrate:fresh --seed

logs:
	docker logs shopping-cart-js -f
