<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');
Route::apiResource('products', 'ProductController');
Route::get('cart/products', 'ShoppingCartController@getProductsInUserCart');
Route::post('cart/{product}/add', 'ShoppingCartController@addProductToUserCart');
Route::post('cart/{product}/update-count', 'ShoppingCartController@updatedProductNumberInUserCart');
Route::post('cart/{product}/remove', 'ShoppingCartController@removeProductInUserCart');