<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateXShoppingCartProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('x_shopping_cart_product', function (Blueprint $table) {
            $table->unsignedInteger('shopping_cart_id');
            $table->unsignedInteger('product_id');
            $table->integer('count')->default(1);
            $table->primary(['shopping_cart_id', 'product_id'], 'shopping_cart_product_primary');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('x_shopping_cart_product');
    }
}
